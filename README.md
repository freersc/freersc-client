# FreeRSC Client

This is the FreeRSC client for playing FreeRSC!

## Usage

Just run Open_RSC_Client.jar!

## License

See LICENSE.txt.

## Credits

FreeRSC is a direct fork of OpenRSC which is available here: https://gitlab.com/open-runescape-classic/core
